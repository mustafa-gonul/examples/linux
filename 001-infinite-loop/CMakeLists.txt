cmake_minimum_required(VERSION 2.8)

# Project
project(002-infinite-loop)

# Executable source files
set(executable_SOURCES src/main.c)

# Executable
add_executable(executable ${executable_SOURCES})

# Install
install(TARGETS executable DESTINATION bin)
