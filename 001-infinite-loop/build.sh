#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd -P)"
NAME="$(basename "$DIR")"

echo "DIR:    ${DIR}"
echo "NAME:   ${NAME}"

cd $DIR

rm -Rf build
rm -Rf install

mkdir build
mkdir install

cd build

# Default: /usr/local
# cmake ..

# Custom: ../install
cmake .. -DCMAKE_INSTALL_PREFIX=../install

## Verbose on
# cmake .. -DCMAKE_INSTALL_PREFIX=../install -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON
# make VERBOSE=1

make clean
make
make install

../install/bin/${NAME}
